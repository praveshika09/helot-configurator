from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(806, 458)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(806, 458))
        MainWindow.setMaximumSize(QtCore.QSize(806, 458))
        MainWindow.setFocusPolicy(QtCore.Qt.NoFocus)
        icon = QtGui.QIcon.fromTheme("EAC")
        MainWindow.setWindowIcon(icon)
        MainWindow.setIconSize(QtCore.QSize(30, 10))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.fileopenButton = QtWidgets.QPushButton(self.centralwidget)
        self.fileopenButton.setGeometry(QtCore.QRect(10, 10, 81, 31))
        self.fileopenButton.setObjectName("fileopenButton")
        self.fileopenButton.clicked.connect(self.getfiles)

        self.configfilepathTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.configfilepathTextEdit.setGeometry(QtCore.QRect(120, 10, 391, 31))
        self.configfilepathTextEdit.setObjectName("configfilepathTextEdit")

        self.saveconfigButton = QtWidgets.QPushButton(self.centralwidget)
        self.saveconfigButton.setGeometry(QtCore.QRect(550, 10, 101, 31))
        self.saveconfigButton.setObjectName("saveconfigButton")

        self.reloadlogstashButton = QtWidgets.QPushButton(self.centralwidget)
        self.reloadlogstashButton.setGeometry(QtCore.QRect(660, 10, 131, 31))
        self.reloadlogstashButton.setObjectName("reloadlogstashButton")

        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(10, 50, 781, 361))
        self.tabWidget.setObjectName("tabWidget")
        self.input_tab = QtWidgets.QWidget()
        self.input_tab.setObjectName("input_tab")

        self.input_tab_pushconfig = QtWidgets.QPushButton(self.input_tab)
        self.input_tab_pushconfig.setGeometry(QtCore.QRect(668, 10, 103, 31))
        self.input_tab_pushconfig.setObjectName("input_tab_pushconfig")
        self.input_tab_pushconfig.clicked.connect(self.pushInputConfig)

        self.input_tab_pulledconfigtext = QtWidgets.QTextBrowser(self.input_tab)
        self.input_tab_pulledconfigtext.setGeometry(QtCore.QRect(10, 40, 371, 281))
        self.input_tab_pulledconfigtext.setObjectName("input_tab_pulledconfigtext")

        self.input_tab_newconfigtext = QtWidgets.QTextEdit(self.input_tab)
        self.input_tab_newconfigtext.setGeometry(QtCore.QRect(400, 40, 371, 281))
        self.input_tab_newconfigtext.setObjectName("input_tab_newconfigtext")
        self.tabWidget.addTab(self.input_tab, "")

        self.filter_tab = QtWidgets.QWidget()
        self.filter_tab.setObjectName("filter_tab")
        self.filter_tab_newconfigtext = QtWidgets.QTextEdit(self.filter_tab)
        self.filter_tab_newconfigtext.setGeometry(QtCore.QRect(400, 40, 371, 281))
        self.filter_tab_newconfigtext.setObjectName("filter_tab_newconfigtext")

        self.filter_tab_pushconfig = QtWidgets.QPushButton(self.filter_tab)
        self.filter_tab_pushconfig.setGeometry(QtCore.QRect(668, 10, 103, 31))
        self.filter_tab_pushconfig.setObjectName("filter_tab_pushconfig")
        self.filter_tab_pushconfig.clicked.connect(self.pushFilterConfig)

        self.filter_tab_pulledconfigtext = QtWidgets.QTextBrowser(self.filter_tab)
        self.filter_tab_pulledconfigtext.setGeometry(QtCore.QRect(10, 40, 371, 281))
        self.filter_tab_pulledconfigtext.setObjectName("filter_tab_pulledconfigtext")

        self.tabWidget.addTab(self.filter_tab, "")
        self.outputtab = QtWidgets.QWidget()
        self.outputtab.setObjectName("outputtab")

        self.output_tab_newconfigtext = QtWidgets.QTextEdit(self.outputtab)
        self.output_tab_newconfigtext.setGeometry(QtCore.QRect(400, 40, 371, 281))
        self.output_tab_newconfigtext.setObjectName("output_tab_newconfigtext")

        self.output_tab_pushconfig = QtWidgets.QPushButton(self.outputtab)
        self.output_tab_pushconfig.setGeometry(QtCore.QRect(668, 10, 103, 31))
        self.output_tab_pushconfig.setObjectName("output_tab_pushconfig")
        self.output_tab_pushconfig.clicked.connect(self.pushOutputConfig)

        self.output_tab_pulledconfigtext = QtWidgets.QTextBrowser(self.outputtab)
        self.output_tab_pulledconfigtext.setGeometry(QtCore.QRect(10, 40, 371, 281))
        self.output_tab_pulledconfigtext.setObjectName("output_tab_pulledconfigtext")
        self.tabWidget.addTab(self.outputtab, "")

        self.grok_tab = QtWidgets.QWidget()
        self.grok_tab.setObjectName("grok_tab")
        self.grok_tab_newconfigtext = QtWidgets.QTextEdit(self.grok_tab)
        self.grok_tab_newconfigtext.setGeometry(QtCore.QRect(400, 40, 371, 281))
        self.grok_tab_newconfigtext.setObjectName("grok_tab_newconfigtext")

        self.grok_tab_pushconfig = QtWidgets.QPushButton(self.grok_tab)
        self.grok_tab_pushconfig.setGeometry(QtCore.QRect(670, 10, 103, 31))
        self.grok_tab_pushconfig.setObjectName("grok_tab_pushconfig")

        self.grok_tab_pulledconfigtext = QtWidgets.QTextBrowser(self.grok_tab)
        self.grok_tab_pulledconfigtext.setGeometry(QtCore.QRect(10, 40, 371, 281))
        self.grok_tab_pulledconfigtext.setObjectName("grok_tab_pulledconfigtext")

        self.if_else_combo = QtWidgets.QComboBox(self.grok_tab)
        self.if_else_combo.setGeometry(QtCore.QRect(10, 10, 241, 31))
        self.if_else_combo.setObjectName("if_else_combo")
        self.mutate_grok_combo = QtWidgets.QComboBox(self.grok_tab)
        self.mutate_grok_combo.setGeometry(QtCore.QRect(400, 10, 231, 31))
        self.mutate_grok_combo.setObjectName("mutate_grok_combo")
        self.tabWidget.addTab(self.grok_tab, "")

        self.grok_tab_pushconfig.clicked.connect(self.pushFilterConfig)
        if_else_combo_list = ['351s_history', '351s_events']
        mutate_grok_combo_list = ['mutate', 'grok']
        self.if_else_combo.addItems(if_else_combo_list)
        self.mutate_grok_combo.addItems(mutate_grok_combo_list)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 806, 26))
        self.menubar.setObjectName("menubar")
        self.menuConfig_Updater = QtWidgets.QMenu(self.menubar)
        self.menuConfig_Updater.setObjectName("menuConfig_Updater")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")

        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionExit = QtWidgets.QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        self.actionAbout = QtWidgets.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        self.menuConfig_Updater.addAction(self.actionExit)
        self.menuHelp.addAction(self.actionAbout)
        self.menubar.addAction(self.menuConfig_Updater.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        # Function to convert

    def listToString(self, s):
        # initialize an empty string
        str1 = "\n"
        # return string
        newstr = str1.join(s)
        new_str = newstr.replace('{SPACE}', '')
        return new_str

    def getfiles(self):
        filename = QFileDialog.getOpenFileName()
        path = filename[0]
        self.configfilepathTextEdit.setPlainText(path)

        with open(path, "r") as f:
            text = f.read()
            self.configParser(text)
            f.close()

    def configParser(self, value):
        s = value
        startinput = s.find("input {") + len("input {")
        endinput = s.find("}\nfilter")
        inputstring = s[startinput:endinput]
        # print(inputstring)
        self.input_tab_pulledconfigtext.setText(inputstring)
        self.input_tab_newconfigtext.setText(inputstring)

        startfilter = s.find("filter {") + len("filter {")
        endfilter = s.find("}\noutput")
        filterstring = s[startfilter:endfilter]
        # print(filterstring)
        self.filter_tab_pulledconfigtext.setText(filterstring)
        self.filter_tab_newconfigtext.setText(filterstring)

        startoutput = s.find("output {") + len("output {")
        endoutput = s.find("\n}\n}")
        outputstring = s[startoutput:endoutput]
        # print(outputstring)
        self.output_tab_pulledconfigtext.setText(outputstring)
        self.output_tab_newconfigtext.setText(outputstring)

        ifelsecombotext = self.if_else_combo.currentText()
        mutategrokcombotext = self.mutate_grok_combo.currentText()

        startgroktxt = filterstring
        if (ifelsecombotext == "351s_history"):
            starthetext = startgroktxt.find("if ([fields][log_type] == \"351s_history\") {") + len(
                "if ([fields][log_type] == \"351s_history\") {")
            endhetext = startgroktxt.find("else if ([fields][log_type] == \"351s_events\") {")
            hetext = startgroktxt[starthetext:endhetext]
            print("this is 315s_history")
            # print(hetext)
            if mutategrokcombotext == "mutate":
                startmgtext = hetext.find("mutate {") + len("mutate {")
                endmgtext = hetext.find("}")
                mgtext = hetext[startmgtext:endmgtext]
                print(ifelsecombotext, " + ", mutategrokcombotext)
                # print(mgtext)

                # self.convertstrtoli(mgtext)
                x = mgtext.split("%")
                print(*x, sep="\n")
                self.grok_tab_pulledconfigtext.setText(mgtext)
                self.grok_tab_newconfigtext.setText(mgtext)

            elif mutategrokcombotext == "grok":
                # print(hetext)
                startmgtext = hetext.find("grok {") + len("grok {")
                endmgtext = hetext.find("}\n}")
                mgtext = hetext[startmgtext:endmgtext]
                # print(mgtext)
                # print(ifelsecombotext, " + ", mutategrokcombotext)
                # # print(mgtext)
                # self.grok_tab_pulledconfigtext.setText(mgtext)
                # self.grok_tab_newconfigtext.setText(mgtext)
                # # self.convertstrtoli(mgtext)
                x = mgtext.split("%")
                mgtext = self.listToString(x)

                # print(*x, sep="\n")
                # mgtext = 'xyz'
        elif ifelsecombotext == "351s_events":
            starthetext = startgroktxt.find("else if ([fields][log_type] == \"351s_events\") {") + len(
                "else if ([fields][log_type] == \"351s_events\") {")
            endhetext = startgroktxt.find("\n}\n}")
            hetext = startgroktxt[starthetext:endhetext]
            print("this is 315s_events")

            # print(hetext)

            if mutategrokcombotext == "mutate":
                startmgtext = hetext.find("mutate {") + len("mutate {")
                endmgtext = hetext.find("}")
                mgtext = hetext[startmgtext:endmgtext]
                print(ifelsecombotext, " + ", mutategrokcombotext)
                # print(mgtext)
                self.grok_tab_pulledconfigtext.setText(mgtext)
                self.grok_tab_newconfigtext.setText(mgtext)
                # self.convertstrtoli(mgtext)
                x = mgtext.split("%")
                print(*x, sep="\n")

            elif mutategrokcombotext == "grok":
                startmgtext = hetext.find("grok {") + len("grok {")
                endmgtext = hetext.find("}\n}")
                mgtext = hetext[startmgtext:endmgtext]
                print(ifelsecombotext, " + ", mutategrokcombotext)
                self.grok_tab_pulledconfigtext.setText(mgtext)
                self.grok_tab_newconfigtext.setText(mgtext)
                # self.convertstrtoli(mgtext)
                x = mgtext.split("%")
                print(*x, sep="\n")
        # print(mgtext)
        self.grok_tab_pulledconfigtext.setText(mgtext)
        self.grok_tab_newconfigtext.setText(mgtext)

    def pushInputConfig(self):
        newVal = self.input_tab_newconfigtext.toPlainText()
        oldVal = self.input_tab_pulledconfigtext.toPlainText()
        path = self.configfilepathTextEdit.toPlainText()
        inputFile = open(path).read()
        outputFile = open(path, 'w')
        replacements = {oldVal: newVal}
        for i in replacements.keys():
            inputFile = inputFile.replace(i, replacements[i])
        outputFile.write(inputFile)
        outputFile.close

        self.reloadFileValues()

    def pushFilterConfig(self):
        newVal = self.filter_tab_newconfigtext.toPlainText()
        oldVal = self.filter_tab_pulledconfigtext.toPlainText()
        path = self.configfilepathTextEdit.toPlainText()
        inputFile = open(path).read()
        outputFile = open(path, 'w')
        replacements = {oldVal: newVal}
        for i in replacements.keys():
            inputFile = inputFile.replace(i, replacements[i])
        outputFile.write(inputFile)
        outputFile.close

        self.reloadFileValues()

    def pushOutputConfig(self):
        newVal = self.output_tab_newconfigtext.toPlainText()
        oldVal = self.output_tab_pulledconfigtext.toPlainText()
        path = self.configfilepathTextEdit.toPlainText()
        inputFile = open(path).read()
        outputFile = open(path, 'w')
        replacements = {oldVal: newVal}
        for i in replacements.keys():
            inputFile = inputFile.replace(i, replacements[i])
        outputFile.write(inputFile)
        outputFile.close

        self.reloadFileValues()

    def pushGrokConfig(self):
        newVal = self.grok_tab_newconfigtext.toPlainText()
        oldVal = self.grok_tab_pulledconfigtext.toPlainText()
        path = self.configfilepathTextEdit.toPlainText()
        inputFile = open(path).read()
        outputFile = open(path, 'w')
        replacements = {oldVal: newVal}
        for i in replacements.keys():
            inputFile = inputFile.replace(i, replacements[i])
        outputFile.write(inputFile)
        outputFile.close

        self.reloadFileValues()

    def reloadFileValues(self):
        path = self.configfilepathTextEdit.toPlainText()
        with open(path, "r") as f:
            # print('i am going to reload the fields')
            text = f.read()
            if text == "":
                print('there is read path error')
            self.configParser(text)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "HELOT - Logstash Configurator v0.1a (EAC, UA Little Rock)"))
        self.fileopenButton.setText(_translate("MainWindow", "Open"))
        self.saveconfigButton.setText(_translate("MainWindow", "Save Config."))
        self.reloadlogstashButton.setText(_translate("MainWindow", "Reload Logstash"))
        self.input_tab_pushconfig.setText(_translate("MainWindow", "Push Config."))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.input_tab), _translate("MainWindow", "Input"))
        self.filter_tab_pushconfig.setText(_translate("MainWindow", "Push Config."))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.filter_tab), _translate("MainWindow", "Filter"))
        self.output_tab_pushconfig.setText(_translate("MainWindow", "Push Config."))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.outputtab), _translate("MainWindow", "Output"))
        self.grok_tab_pushconfig.setText(_translate("MainWindow", "Push Config."))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.grok_tab), _translate("MainWindow", "Grok Filter"))
        self.menuConfig_Updater.setTitle(_translate("MainWindow", "File"))
        self.menuHelp.setTitle(_translate("MainWindow", "Help"))

        self.actionExit.setText(_translate("MainWindow", "Exit"))
        self.actionAbout.setText(_translate("MainWindow", "About"))


# import EAC-Icon-Test-1_rc

def main():
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
